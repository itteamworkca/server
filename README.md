# IT TeamWork CA - Spring Boilerplate

> Hacking is creative problem solving

## Getting start with *Spring Boot*

Access [Spring Initializr](https://start.spring.io/) and set up a project using the following options:

**Group**: ca.itteamwork
**Artifact**: blank-project
**Name**: IT TeamWork CA Blank Project
**Description**: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
**Package Name**: ca.itteamwork
**Packaging**: war
**Java Version**: 1.8
**Language**: Java

Choose between **maven** or **gradle** project and the latest Spring Boot stable version.

Suggested dependencies:

**Core**: Security, DevTools
**Web**: Web
**SQL**: JPA, PostgreSQL
