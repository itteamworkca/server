package ca.itteamwork.data.pushnotification.service;

import ca.itteamwork.BlankProjectApplication;
import ca.itteamwork.domain.Device;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.concurrent.Future;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BlankProjectApplication.class)
@WebAppConfiguration
public class PushNotificationServiceTest {

    @Autowired
    private PushNotificationService pushNotificationService;

    @Mock
    private Device device;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldSendPushNotificationToIonicDash() throws Exception {
        Mockito.when(device.getToken()).thenReturn("DEV-b364d42b-f7f7-4b59-894f-29e49009bdb9");

        Future<Boolean> notification = pushNotificationService.sendPushNotification(device);
        Boolean result = notification.get();

        Assert.assertTrue(result);
    }

}
