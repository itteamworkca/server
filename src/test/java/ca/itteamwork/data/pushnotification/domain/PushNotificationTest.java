package ca.itteamwork.data.pushnotification.domain;

import ca.itteamwork.BlankProjectApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.StringUtils;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BlankProjectApplication.class)
@WebAppConfiguration
public class PushNotificationTest {

    @Mock
    private PushNotification pushNotification;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
;    }

    @Test
    public void shouldWriteJSONCorrectly() throws Exception {
        Mockito.when(pushNotification.getTitle()).thenReturn("title");
        Mockito.when(pushNotification.getMessage()).thenReturn("message");
        Mockito.when(pushNotification.getProfile()).thenReturn("profile");
        Mockito.when(pushNotification.getTokens()).thenReturn(new String[]{"a", "b", "c"});
        Mockito.when(pushNotification.toString()).thenCallRealMethod();

        String actual = pushNotification.toString();
        String excepted = StringUtils.trimAllWhitespace(
            "{" +
                "\"tokens\": [\"a\", \"b\", \"c\", \"d\"]," +
                "\"profile\": \"profile\"," +
                "\"notification\": {" +
                    "\"title\": \"title\"," +
                    "\"message\": \"message\"" +
                "}" +
            "}"
        );

        Assert.assertEquals(excepted, actual);
    }

}
