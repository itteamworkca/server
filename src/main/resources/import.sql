INSERT INTO people (name, email_person) VALUES ("Bruno", "bruno@teste");
INSERT INTO people (name, email_person) VALUES ("Lais", "lais@teste");
INSERT INTO people (name, email_person) VALUES ("Poiani", "poiani@teste");
INSERT INTO people (name, email_person) VALUES ("Leo", "leo@teste");

INSERT INTO person_id VALUES (1, 2);
INSERT INTO person_id VALUES (1, 3);
INSERT INTO person_id VALUES (1, 4);
INSERT INTO person_id VALUES (2, 3);
INSERT INTO person_id VALUES (2, 4);
INSERT INTO person_id VALUES (3, 4);

INSERT INTO chat (email1, email2) VALUES ("leo@teste", "bruno@teste"); 
INSERT INTO chat (email1, email2) VALUES ("leo@teste", "lais@teste");
INSERT INTO chat (email1, email2) VALUES ("leo@teste", "poiani@teste");
INSERT INTO chat (email1, email2) VALUES ("poiani@teste", "lais@teste");
INSERT INTO chat (email1, email2) VALUES ("bruno@teste", "poiani@teste");
INSERT INTO chat (email1, email2) VALUES ("bruno@teste", "lais@teste");

INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e bruno', 1, 1, '1', '1');
INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e bruno n�o lida - owner leo', 0, 1, '1', '4');
INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e bruno n�o lida owner bruno', 0, 1, '1', '1');

INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e LAIS', 1, 1, '2', '2');
INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e LAIS n�o lida - owner leo', 0, 1, '2', '4');
INSERT INTO message (`message`, `seen`, `sent`, `chat_id`, `owner_id`) VALUES ('msg entre leo e LAIS n�o lida owner LAIS', 0, 1, '2', '2');

INSERT INTO chat_messages VALUES('1', '3');
INSERT INTO chat_messages VALUES('1', '7');
INSERT INTO chat_messages VALUES('2', '6');
INSERT INTO chat_messages VALUES('2', '8');

