package ca.itteamwork.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Person;
import ca.itteamwork.repository.ChatRepository;
import ca.itteamwork.repository.MessageRepository;
import ca.itteamwork.repository.PersonRepository;

@Service
@Transactional
public class PersonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MessageRepository messageRepository;

    public List<Person> findAll() {
        LOGGER.info("Find all Persons");
        return personRepository.findAll();
    }


    public Person createOrUpdate(Person person) {
        LOGGER.info("Creating Person");

        Person entity;
        Person original = personRepository.findOneByEmail(person.getEmail());

        if (original != null) {
            LOGGER.debug("Updating Person with same email: {}", original);
            entity = merge(original, person);
        } else {
            entity = person;
        }

        personRepository.save(entity);

        LOGGER.debug("Person saved: {}", entity);

        return entity;
    }

    public boolean addContact(Long id, Person person) {
        LOGGER.info("Add contact...");
        Person contact = personRepository.findOneByEmail(person.getEmail());
        Person p = personRepository.findOne(id);
        if(contact == null){ //not on the DB
        	LOGGER.info("User not registered on DB");
        	return false;
        }
        else{
        	int exists = personRepository.checkExistingContact(id, contact.getId());
        	if(exists == 0){
        		LOGGER.info("Inserting contact...");
            	contact.setUnreadMessages((long) 0);
                p.getContacts().add(contact);
                personRepository.save(p);

                LOGGER.debug("Created: {}", contact);
        	}
        	else
        		LOGGER.info("Contact already exists");

            return true;
        }
    }

    public Person find(Long id) {
        LOGGER.info("Find Person - {}", id);
        Person person = personRepository.findOne(id);

        if (person == null) {
            LOGGER.info("Person {} not found", id);
            return null;
        }

        LOGGER.debug("Retrieved: {}", person);

        return person;
    }

    public Person update(Person original, Person current) {
        LOGGER.info("Updating Person - {}", original.getId());

        Person person  = merge(original, current);
        personRepository.save(person);

        LOGGER.debug("Updated: {}", person);

        return person;
    }

    public Person delete(Long id) {
        LOGGER.info("Removing Person - {}", id);

        Person person = personRepository.findOne(id);

        if (person == null) {
            LOGGER.info("Person {} not found", id);
            return null;
        }

        personRepository.save(person);

        LOGGER.debug("Removed: {}", person);

        return person;
    }

    private Person merge(Person original, Person current) {
        if (!StringUtils.isEmpty(current.getName())) {
            original.setName(current.getName());
        }

        if (!StringUtils.isEmpty(current.getPictureProfileUrl())) {
            original.setPictureProfileUrl(current.getPictureProfileUrl());
        }

        if (!StringUtils.isEmpty(current.getSocialNetwork())) {
            original.setSocialNetwork(current.getSocialNetwork());
        }

        if (!StringUtils.isEmpty(current.getSocialId())) {
            original.setSocialId(current.getSocialId());
        }

        return original;
    }

	public Person getContacts(Long id) {
		Person p = personRepository.getContactsByPerson(id);

        if (p == null) {
            return find(id);
        }

		for(Person c : p.getContacts()){
			LOGGER.debug("email 1: ", p.getEmail());
            LOGGER.debug("email 2: ", c.getEmail());

            Chat chat = chatRepository.checkExistingChat(p.getEmail(), c.getEmail());

            if(chat != null){
				int unReadMessages = messageRepository.getUnreadMessages(chat.getId());
				c.setUnreadMessages((long)unReadMessages);
			}
		}

		return p;
	}
}
