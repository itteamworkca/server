package ca.itteamwork.service;

import ca.itteamwork.domain.Device;
import ca.itteamwork.repository.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DeviceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);

    @Autowired
    private DeviceRepository deviceRepository;

    public List<Device> findAll() {
        LOGGER.info("Find all Devices");
        return deviceRepository.findAllByActiveTrue();
    }

    public Device create(Device device) {
        LOGGER.info("Creating Device");
        deviceRepository.save(device);

        LOGGER.debug("Created: {}", device);

        return device;
    }

    public Device findByToken(String token) {
        LOGGER.info("Find Device by Token - {}", token);
        Device device = deviceRepository.findOneByTokenAndActiveTrue(token);

        if (device == null) {
            LOGGER.info("Device token {} not found", token);
            return null;
        }

        LOGGER.debug("Retrieved: {}", device);

        return device;
    }

    public Device delete(Long id) {
        LOGGER.info("Removing Device - {}", id);

        Device device = deviceRepository.findOneByIdAndActiveTrue(id);

        if (device == null) {
            LOGGER.info("Device {} not found", id);
            return null;
        }

        device.setActive(false);
        deviceRepository.save(device);

        LOGGER.debug("Removed: {}", device);

        return device;
    }
}
