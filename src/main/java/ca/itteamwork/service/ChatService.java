package ca.itteamwork.service;

import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.repository.ChatRepository;
import ca.itteamwork.repository.PersonRepository;

@Service
@Transactional
public class ChatService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatService.class);

    @Autowired
    private ChatRepository chatRepository;
    @Autowired
    private PersonRepository personRepository;

    public List<Chat> findAll() {
        LOGGER.info("Find all Chats");
        return chatRepository.findAll();
    }

    public Chat create(String emailPerson, String emailContact) {
        LOGGER.info("Creating Chat");

        Chat chat = chatRepository.checkExistingChat(emailPerson, emailContact);
        
        if(chat != null){
        	LOGGER.info("Chat already exists");
        	return chat;
        }
        else{
        	
        	chat = new Chat();
        	chat.setContact2(personRepository.findOneByEmail(emailContact));
        	chat.setContact1(personRepository.findOneByEmail(emailPerson));
        	chat.setCreateAt(DateTime.now());
        	chatRepository.save(chat);
            LOGGER.debug("Created: {}", chat);
        	return chat;
        }
    }
    
    public List<Chat> getChats(Long id) {
		LOGGER.info("Returning chats for id = " +id);
		return null;//chatRepository.getChatsByPerson(id);
	}

    public Chat find(Long id) {
        LOGGER.info("Find Chat - {}", id);
        Chat chat = chatRepository.findOne(id);

        if (chat == null) {
            LOGGER.info("Chat {} not found", id);
            return null;
        }

        LOGGER.debug("Retrieved: {}", chat);

        return chat;
    }

//    public Chat update(Chat original, Chat current) {
//        LOGGER.info("Updating Chat - {}", original.getId());
//
//        Chat Chat  = merge(original, current);
//        chatRepository.save(chat);
//
//        LOGGER.debug("Updated: {}", chat);
//
//        return Chat;
//    }

    public Chat delete(Long id) {
        LOGGER.info("Removing Chat - {}", id);

        Chat chat = null;//chatRepository.findOneByIdAndActiveTrue(id);

        if (chat == null) {
            LOGGER.info("Chat {} not found", id);
            return null;
        }

        chatRepository.delete(chat);

        LOGGER.debug("Removed: {}", chat);

        return chat;
    }

//    private Chat merge(Chat original, Chat current) {
//        if (!StringUtils.isEmpty(current.getName())) {
//            original.setName(current.getName());
//        }
//
//        return original;
//    }
}
