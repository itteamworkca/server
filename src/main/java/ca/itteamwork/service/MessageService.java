package ca.itteamwork.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Emails;
import ca.itteamwork.domain.Message;
import ca.itteamwork.repository.ChatRepository;
import ca.itteamwork.repository.MessageRepository;

@Service
@Transactional
public class MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ChatRepository chatRepository;

    public List<Message> findAll() {
        LOGGER.info("Find all Messages");
        return null;//messageRepository.findAllByActiveTrue();
    }

    public Message sendMessage(Message message, Chat chat) {
        LOGGER.info("Creating Message");

        messageRepository.save(message);

        LOGGER.debug("Created: {}", message);

        return message;
    }

    public Chat getMessages(Emails emails) {
    	Chat chat  = chatRepository.checkExistingChat(emails.getEmail1(), emails.getEmail2());
    	List<Message> messages = messageRepository.findAllByChatIdOrderByTimestampAsc(chat.getId());
    	System.out.println("teste message: " +messages.size());
    	chat.setMessages(messages);
    	return chat;
	}

    public Message find(Long id) {
        LOGGER.info("Find Message - {}", id);
        Message model = null;//messageRepository.findOneByIdAndActiveTrue(id);
        if (model == null) {
            LOGGER.info("Message {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Message delete(Long id) {
        LOGGER.info("Removing Message - {}", id);
        Message message = null;//messageRepository.findOneByIdAndActiveTrue(id);
        if (message == null) {
            LOGGER.info("Message {} not found", id);
            return null;
        }
        messageRepository.delete(message);
        LOGGER.debug("Removed: {}", message);
        return message;
    }

	public Message saveMessage(Message message) {
		LOGGER.info("Saving message...");

        message.setTimestamp(DateTime.now());
        message.setSeen(false);
        message.setSent(true);

        Message entity = messageRepository.save(message);

        LOGGER.info("Message saved");

        return entity;
    }

	/**Listen for new messages*/
	public List<Chat> getNewMessages() {
		List<Message> messagesNew = messageRepository.findAllBySeenFalse();
		List<Chat> chats = new ArrayList<Chat>();
		Chat chat;
		Map<Long, Chat> mapChat = new HashMap<Long, Chat>();

		for(Message m : messagesNew){

			chat = new Chat();
			chat = chatRepository.findOne(m.getChatId());

			if(!mapChat.containsKey(chat.getId())){
				chat.getMessages().add(m);
				mapChat.put(chat.getId(), chat);
			}
			else{
				chat = mapChat.get(chat.getId());
				chat.getMessages().add(m);
			}
		}
		chats.addAll(mapChat.values());
		return chats;
	}

	/**Listen for new messages*/
	public List<Chat> getNewMessages(Long id) {

		messageRepository.cleanRelationshipTable();
		List<Message> messagesNew = messageRepository.findAllBySeenFalseAndOwnerId(id);
		List<Chat> chats = new ArrayList<Chat>();
		Chat chat;
		Map<Long, Chat> mapChat = new HashMap<Long, Chat>();

		for(Message m : messagesNew){

			chat = new Chat();
			chat = chatRepository.findOne(m.getChatId());

			if(!mapChat.containsKey(chat.getId())){
				chat.getMessages().add(m);
				mapChat.put(chat.getId(), chat);
			}
			else{
				chat = mapChat.get(chat.getId());
				chat.getMessages().add(m);
			}
		}
		chats.addAll(mapChat.values());
		return chats;
	}

}
