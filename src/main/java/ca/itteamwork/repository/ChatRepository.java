package ca.itteamwork.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ca.itteamwork.domain.Chat;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {

	@Modifying
	@Query(value = "INSERT INTO people_chat (person_PERSON_ID, chat_CHAT_ID) VALUES (?1, ?2)", nativeQuery = true)
	 void insertPersonRelationship(Long personId, Long chatId);

	@Query(value = "SELECT * from chat c where c.email1 = ?1 and c.email2 = ?2 OR c.email1 = ?2 and c.email2 = ?1", nativeQuery = true)
	Chat checkExistingChat(String emailPerson, String emailContact);

	List<Chat> findAllByContact1(Long id);

	List<Chat> findAllByContact1Id(Long id);
}
