package ca.itteamwork.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ca.itteamwork.domain.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	Person findOneByEmail(String emailPerson);

	@Query(value="SELECT distinct p.* from people p left join person_id pi ON (p.person_id = pi.person_id) where pi.person_id = ?1", nativeQuery = true)
	Person getContactsByPerson(Long id);

	@Query(value="SELECT count(*) from person_id where person_id = ?1 and person_id1 = ?2", nativeQuery=true)
	int checkExistingContact(Long id, Long id2);
}
