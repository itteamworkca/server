package ca.itteamwork.repository;

import ca.itteamwork.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    List<Device> findAllByActiveTrue();

    Device findOneByIdAndActiveTrue(Long id);

    Device findOneByTokenAndActiveTrue(String token);
}
