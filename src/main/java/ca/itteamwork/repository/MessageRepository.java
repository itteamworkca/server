package ca.itteamwork.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

	@Query(value = "SELECT c.* from chat c LEFT JOIN message m ON (c.chat_id = m.chat_id) "+
	 "where c.email1 = ?1 and c.email2 = ?2 OR c.email1 = ?2 and c.email2 = ?1", nativeQuery = true)
	Chat getMessagesByChat(String email1, String email2);

	@Query(value = "SELECT c.* from chat c "+
	 "where c.email1 = ?1 and c.email2 = ?2 OR c.email1 = ?2 and c.email2 = ?1", nativeQuery = true)
	Chat getChat(String email1, String email2);

	List<Message> findAllByChatId(Long id);

	List<Message> findAllBySeenFalse();

	@Query(value = "SELECT count(*) FROM message where seen = false and chat_id = ?1", nativeQuery = true)
	int getUnreadMessages(Long id);

	List<Message> findAllBySeenFalseAndOwnerId(Long id);

	@Modifying
	@Query(value = "DELETE FROM chat_messages", nativeQuery = true)
	void cleanRelationshipTable();

	List<Message> findAllByChatIdOrderByTimestampAsc(Long id);

}
