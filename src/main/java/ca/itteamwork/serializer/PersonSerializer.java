package ca.itteamwork.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import ca.itteamwork.domain.Person;

public class PersonSerializer extends JsonSerializer<Person>{

	@Override
	public void serialize(Person value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {
		try {
			gen.writeStartObject();
			gen.writeNumberField("id", value.getId());
			gen.writeStringField("name", value.getName());
			gen.writeStringField("email", value.getEmail());
            gen.writeStringField("picture_profile_url", value.getPictureProfileUrl());

			if(value.getUnreadMessages() != null)
				gen.writeNumberField("unreadMessages", value.getUnreadMessages());
			if(value.getChat() != null)
				gen.writeObjectField("chats", value.getChat());


			if(value.getContacts() != null){
				List<Person> conts = new ArrayList<Person>();
				for(Person p : value.getContacts()){
					Person c = new Person();
					c.setId(p.getId());
					c.setEmail(p.getEmail());
					c.setName(p.getName());
                    c.setPictureProfileUrl(p.getPictureProfileUrl());
					c.setUnreadMessages(p.getUnreadMessages());
					conts.add(c);
				}
				gen.writeObjectField("contacts", conts);
			}

	        gen.writeEndObject();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
