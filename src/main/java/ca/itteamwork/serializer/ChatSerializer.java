package ca.itteamwork.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Person;

public class ChatSerializer extends JsonSerializer<Chat>{

	@Override
	public void serialize(Chat value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {
		gen.writeStartObject();
		gen.writeNumberField("id", value.getId());
		gen.writeStringField("createAt", String.valueOf(value.getCreateAt()));

		Person c1 = new Person();
		c1.setId(value.getContact1().getId());
		c1.setName(value.getContact1().getName());
		c1.setEmail(value.getContact1().getEmail());
        c1.setPictureProfileUrl(value.getContact1().getPictureProfileUrl());

		Person c2 = new Person();
		c2.setId(value.getContact2().getId());
		c2.setName(value.getContact2().getName());
		c2.setEmail(value.getContact2().getEmail());
        c2.setPictureProfileUrl(value.getContact2().getPictureProfileUrl());
		
		gen.writeObjectField("contact1", c1);
		gen.writeObjectField("contact2", c2);
		gen.writeObjectField("messages", value.getMessages());
		gen.writeEndObject();
	}

}
