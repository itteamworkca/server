package ca.itteamwork.data.pushnotification.domain;

import ca.itteamwork.data.pushnotification.serializer.PushNotificationSerializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = PushNotificationSerializer.class)
public class PushNotification {
    private String[] tokens;
    private String profile;
    private Notification notification;

    public PushNotification(String title, String message, String profile, String... tokens) {
        notification = new Notification(title, message);
        setProfile(profile);
        setTokens(tokens);
    }

    public String[] getTokens() {
        return tokens;
    }

    public void setTokens(String[] tokens) {
        this.tokens = tokens;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getMessage() {
        return notification.getMessage();
    }

    public String getTitle() {
        return notification.getTitle();
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "[PushNotification]";
    }
}
