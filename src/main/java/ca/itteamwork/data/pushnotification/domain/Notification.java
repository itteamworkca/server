package ca.itteamwork.data.pushnotification.domain;

class Notification {
    private String message;
    private String title;


    Notification(String title, String message) {
        setTitle(title);
        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
