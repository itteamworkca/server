package ca.itteamwork.data.pushnotification.service;

import ca.itteamwork.data.pushnotification.domain.PushNotification;
import ca.itteamwork.domain.Device;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.concurrent.Future;

@Service
@Transactional
public class PushNotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationService.class);
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Value("${hootchat.pushnotification.title}")
    private String pushNotificationTitle;

    @Value("${hootchat.pushnotification.message}")
    private String pushNotificationMessage;

    @Value("${hootchat.pushnotification.api}")
    private String pushNotificationAPI;

    @Value("${hootchat.pushnotification.token}")
    private String pushNotificationToken;

    @Value("${hootchat.pushnotification.profile}")
    private String pushNotificationProfile;

    @Async
    public Future<Boolean> sendPushNotification(Device device) throws IOException {
        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, getPushNotification(device).toString());

        Request request = new Request.Builder()
            .url(pushNotificationAPI)
            .addHeader("Authorization", "Bearer " + pushNotificationToken)
            .post(body)
            .build();

        Response response;

        try {
            response = client.newCall(request).execute();

            LOGGER.info("PushNotification sent");
            LOGGER.debug("Response: {}", response.body().string());

            return new AsyncResult<>(Boolean.TRUE);
        } catch (IOException e) {
            LOGGER.error("Error on PushNotification");
            return new AsyncResult<>(Boolean.FALSE);
        }
    }

    private PushNotification getPushNotification(Device device) {
        return new PushNotification(pushNotificationTitle, pushNotificationMessage, pushNotificationProfile, device.getToken());
    }
}
