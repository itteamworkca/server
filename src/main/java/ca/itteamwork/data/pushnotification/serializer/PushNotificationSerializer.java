package ca.itteamwork.data.pushnotification.serializer;

import ca.itteamwork.data.pushnotification.domain.PushNotification;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class PushNotificationSerializer extends JsonSerializer<PushNotification> {
    @Override
    public void serialize(PushNotification value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartObject();

        gen.writeFieldName("tokens");
        gen.writeStartArray();

        if (value.getTokens().length > 0) {
            for (String token : value.getTokens()) {
                gen.writeObject(token);
            }
        }

        gen.writeEndArray();

        gen.writeStringField("profile", value.getProfile());

        gen.writeObjectFieldStart("notification");
        gen.writeStringField("title", value.getTitle());
        gen.writeStringField("message", value.getMessage());
        gen.writeEndObject();

        gen.writeEndObject();
    }
}
