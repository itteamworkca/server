package ca.itteamwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@MappedSuperclass
abstract class AbstractEntity {

    private DateTime createdAt;
    private DateTime updatedAt;
    private Boolean active;

    @Column(nullable = false, updatable = false, columnDefinition="datetime default CURRENT_TIMESTAMP")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonIgnore
    public DateTime getCreatedAt() {
        return createdAt;
    }

    private void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonIgnore
    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    private void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Column(nullable = false, columnDefinition = "TINYINT(1) default true")
    @JsonIgnore
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @PrePersist
    private void onCreate() {
        setCreatedAt(new DateTime());
        setActive(true);
    }

    @PreUpdate
    private void onUpdate() {
        setUpdatedAt(new DateTime());
    }
}
