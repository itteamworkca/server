package ca.itteamwork.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ca.itteamwork.serializer.ChatSerializer;

@Entity(name = "chat")
@JsonSerialize(using = ChatSerializer.class)
public class Chat implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private DateTime createAt;
    private Person contact1;
    private Person contact2;
    private List<Message> messages;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CHAT_ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, updatable = false, columnDefinition="datetime default CURRENT_TIMESTAMP")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(DateTime createAt) {
		this.createAt = createAt;
	}

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "email1", referencedColumnName="EMAIL_PERSON")
	public Person getContact1() {
		return contact1;
	}

	public void setContact1(Person contact1) {
		this.contact1 = contact1;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "email2", referencedColumnName="EMAIL_PERSON")
	public Person getContact2() {
		return contact2;
	}

	public void setContact2(Person contact2) {
		this.contact2 = contact2;
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "chat_messages", joinColumns = { @JoinColumn(name = "CHAT_ID") }, inverseJoinColumns = { @JoinColumn(name = "messageId") })
	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

}
