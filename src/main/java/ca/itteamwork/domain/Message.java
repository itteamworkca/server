package ca.itteamwork.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity(name = "message")
public class Message implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
//    private Chat chat;
    private String message;
    private DateTime timestamp;
    private boolean seen;
    private boolean sent;
    private Long ownerId;
    private Long chatId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MESSAGE_ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id){
    	this.id = id;
    }


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	@Column(nullable = false, updatable = false, columnDefinition="datetime default CURRENT_TIMESTAMP")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

//	@OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "PERSON_ID")
//	public Person getOwner() {
//		return owner;
//	}
//
//
//	public void setOwner(Person owner) {
//		this.owner = owner;
//	}

//	@ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "CHAT_ID")
//	@JsonIgnore
//	public Chat getChat() {
//		return chat;
//	}
//
//	public void setChat(Chat chat) {
//		this.chat = chat;
//	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getChatId() {
		return chatId;
	}

	public void setChatId(Long chatId) {
		this.chatId = chatId;
	}

//    @PrePersist
//    private void onCreate() {
//        setTimestamp(new DateTime());
//    }


}
