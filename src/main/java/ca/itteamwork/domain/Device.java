package ca.itteamwork.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity(name = "devices")
public class Device extends AbstractEntity {
    private Long id;
    private String token;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEVICE_ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
