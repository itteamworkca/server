package ca.itteamwork.controller;

import ca.itteamwork.domain.Message;
import ca.itteamwork.service.ChatService;
import ca.itteamwork.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Controller
public class WebSocketController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketController.class);

    @Autowired
    private MessageService messageService;

    @MessageMapping("/send-message/{room}")
    @SendTo("/topic/notification/{room}")
    public Message sendMessage(@DestinationVariable String room, @RequestBody Message message) {
        return messageService.saveMessage(message);
    }

    @EventListener
    private void onConnect(SessionConnectEvent event) {
        LOGGER.warn("TODO set user online"); // TODO set user online
        LOGGER.warn("TODO notify online friends that this user are online"); // TODO notify online friends that this user are online
    }

    @EventListener
    private void onDisconnect(SessionDisconnectEvent event) {
        LOGGER.warn("TODO set user offline"); // TODO set user offline
        LOGGER.warn("TODO notify online friends that this user are offline"); // TODO notify online friends that this user are offline
    }
}
