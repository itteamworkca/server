package ca.itteamwork.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Emails;
import ca.itteamwork.domain.Message;
import ca.itteamwork.service.MessageService;

@RestController
@RequestMapping("/messages")
@CrossOrigin
public class MessageController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Message> list() {
        return messageService.findAll();
    }

	@RequestMapping(value = "/getMessages", method = RequestMethod.POST)
    public ResponseEntity getMessages(@Valid @RequestBody Emails emails) {
        Chat entity = messageService.getMessages(emails);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getNewMessages", method = RequestMethod.GET)
    public ResponseEntity getNewMessages() {
        List<Chat> entity = messageService.getNewMessages();
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getNewMessages/{id}", method = RequestMethod.GET)
    public ResponseEntity getNewMessagesById(@PathVariable("id") Long id) {

        List<Chat> entity = messageService.getNewMessages(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id) {
        Message entity = messageService.delete(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
