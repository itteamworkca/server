package ca.itteamwork.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ca.itteamwork.domain.Chat;
import ca.itteamwork.domain.Emails;
import ca.itteamwork.domain.Chat;
import ca.itteamwork.service.ChatService;

@RestController
@RequestMapping("/chat")
@CrossOrigin
public class ChatController {

    @Autowired
    private ChatService chatService;


    /**FIND ALL*/
    @RequestMapping(method = RequestMethod.GET)
    public List<Chat> list() {
        return chatService.findAll();
    }

    /**CREATE CHAT BETWEEN 2 CONTACTS (owner and the one chosen*/
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getChat", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Emails emails) {
    	System.out.println("id person: " +emails.getEmail1());
    	Chat entity = chatService.create(emails.getEmail1(), emails.getEmail2());
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    /**GET CONTACTS*/
    @RequestMapping(value = "getChats/{id}", method = RequestMethod.GET)
    public List<Chat> getChats(@PathVariable("id") Long id) {
    	return chatService.getChats(id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity retrieve(@PathVariable("id") Long id) {
    	Chat entity = chatService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Chat current) {
    	Chat original = chatService.find(id);

        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Chat entity = null;//contactService.update(original, current);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id) {
    	Chat entity = chatService.delete(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
