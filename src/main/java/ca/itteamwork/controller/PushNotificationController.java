package ca.itteamwork.controller;

import ca.itteamwork.domain.Device;
import ca.itteamwork.service.DeviceService;
import ca.itteamwork.data.pushnotification.service.PushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/push-notification")
@CrossOrigin
public class PushNotificationController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private PushNotificationService pushNotificationService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Device device) throws IOException {
        Device original = deviceService.findByToken(device.getToken());

        if (original != null) {
            return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);
        }

        Device entity = deviceService.create(device);

        pushNotificationService.sendPushNotification(device);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
