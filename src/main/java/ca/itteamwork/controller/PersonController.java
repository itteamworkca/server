package ca.itteamwork.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ca.itteamwork.domain.Person;
import ca.itteamwork.service.PersonService;

@RestController
@RequestMapping("/people")
@CrossOrigin
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Person> list() {
        return personService.findAll();
    }

    /**CREATE PERSON*/
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createPerson(@Valid @RequestBody Person person) {
    	Person entity = personService.createOrUpdate(person);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    /**ADD CONTACT*/
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "addContact/{id}", method = RequestMethod.POST)
    public ResponseEntity addContact(@PathVariable("id") Long id, @Valid @RequestBody Person person) {
    	boolean worked = personService.addContact(id, person);
    	if(worked)
    		return new ResponseEntity<>(HttpStatus.OK);
    	else
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**GET CONTACTS*/
    @RequestMapping(value = "getContacts/{id}", method = RequestMethod.GET)
    public Person getContacts(@PathVariable("id") Long id) {
    	return personService.getContacts(id);
    }

    /**GET CONTACTS*/
    @RequestMapping(value = "getContacts1/{id}", method = RequestMethod.GET)
    public Person getContacts1(@PathVariable("id") Long id) {
    	return personService.find(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity retrieve(@PathVariable("id") Long id) {
    	Person entity = personService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Person current) {
    	Person original = personService.find(id);

        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Person entity = personService.update(original, current);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id) {
    	Person entity = personService.delete(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
