FROM java:8

VOLUME /tmp

ADD build/libs/blank-project-0.0.1.jar blank-project.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/blank-project.jar"]
